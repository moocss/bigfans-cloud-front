/**
 * Created by lichong on 3/5/2018.
 */
import React, {Component} from 'react';

import {Col ,Card , Button} from 'antd';
import './coupon.css';

class CouponItem extends Component {

    state = {
        loading : false,
        loadingMsg : '立即领取',
        btnDisable : false
    }

    enterLoading() {
        this.setState({loading : true , loadingMsg: '领取中'});
    }

    render() {
        return (
            <div className="coupon-item">
                <Card>
                    <Col span={18} style={{borderRight: '2px dashed #74d2d4'}}>
                        <strong className="currency">¥</strong>
                        <strong className="money">3</strong>
                        <span>满100可用</span>
                        <br/>
                        <small className="limit-info">仅可购买索尼数码自营全线产品商品全平台可用</small>
                        <br/>
                        <small className="duration">2016.11.01 00:00-2016.11.12 23:59</small>
                    </Col>
                    <Col span={6} className="text-center" style={{padding : '8px' , lineHeight : '50px'}}>
                        <Button type="primary" disabled={this.state.btnDisable} loading={this.state.loading} onClick={() => this.enterLoading()}>
                            {this.state.loadingMsg}
                        </Button>
                        <span style={{lineHeight : '10px'}}>已领 98 %</span>
                    </Col>
                </Card>
            </div>
        )
    }
}

export default CouponItem;