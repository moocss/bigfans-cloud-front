const StorageUtils = {

	getToken(){
		let storedToken = localStorage.getItem('PortalToken') 
		if(storedToken == null){
			return null;
		}
		let TokenObject = JSON.parse(storedToken)
		let createTime = TokenObject.createTime
		let currentTime = new Date().getTime()
		// expired
		if(createTime + (TokenObject.duration * 1000) < currentTime){
			localStorage.removeItem('PortalToken')
			return null;
		}
		return TokenObject.token;
	},

	setToken(tokenObject){
		let createTime = new Date().getTime()
		tokenObject.createTime = createTime
		localStorage.setItem('PortalToken' , JSON.stringify(tokenObject))
	}
}

export default StorageUtils;
