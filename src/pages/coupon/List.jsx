/**
 * Created by lichong on 3/5/2018.
 */
import React from 'react';

import {Row, Col, Input, Divider, Breadcrumb, Tag, Card} from 'antd';
import TopBar from '../../components/TopBar';
import SearchBar from '../../components/SearchBar'
import TopMenu from '../../components/TopMenu';
import CouponItem from '../../components/CouponItem'

class CouponList extends React.Component {

    state = {
        products: [
            {
                id: '1',
                name: 'aa'
            },
            {
                id:'2',
                name:'hehe'
            }
        ]
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <SearchBar/>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <TopMenu/>
                    <Col span={3}/>
                </Row>
                <Row style={{marginTop: '20px'}}>
                    <Col span={3}/>
                    <Col span={18}>
                        <Row gutter={16}>
                            <Col span={8} style={{marginBottom : '6px'}}>
                                <CouponItem/>
                            </Col>
                            <Col span={8} style={{marginBottom : '6px'}}>
                                <CouponItem/>
                            </Col>
                            <Col span={8} style={{marginBottom : '6px'}}>
                                <CouponItem/>
                            </Col>
                            <Col span={8} style={{marginBottom : '6px'}}>
                                <CouponItem/>
                            </Col>
                            <Col span={8} style={{marginBottom : '6px'}}>
                                <CouponItem/>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        )
    }
}

export default CouponList;