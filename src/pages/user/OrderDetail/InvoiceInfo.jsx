/**
 * Created by lichong on 3/7/2018.
 */
import React from 'react';

import {Row, Col , Card} from 'antd';

class InvoiceInfo extends React.Component {

    render() {
        return (
            <div>
                <Card title="发票信息" bordered={false}>
                    <Row gutter={15}>
                        <Col span={6}>
                            <strong className="pull-right">收货人</strong>
                        </Col>
                        <Col span={18}>
                            <span>bbbbb</span>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={6}>
                            <strong className="pull-right">收货地址</strong>
                        </Col>
                        <Col span={18}>
                            <span>bbbbb</span>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={6}>
                            <strong className="pull-right">手机号码</strong>
                        </Col>
                        <Col span={18}>
                            <span>bbbbb</span>
                        </Col>
                    </Row>
                </Card>
            </div>
        )
    }
}

export default InvoiceInfo;