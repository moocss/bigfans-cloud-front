/**
 * Created by lichong on 2/27/2018.
 */
import React from 'react';

import {Row, Col, Menu, Card, Divider, Dropdown, Icon} from 'antd';
import CouponItem from '../../components/CouponItem'
import TopBar from '../../components/TopBar';
import FootBar from '../../components/FootBar';
import CenterMenu from '../../components/UserCenterMenus'

class MyCoupons extends React.Component {

    render() {
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <TopBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Divider style={{margin: '0'}}/>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <Row gutter={15}>
                            <Col span={5}>
                                <CenterMenu currentMenu="mycoupons"/>
                            </Col>
                            <Col span={19}>
                                <CouponItem/>
                                <CouponItem/>
                                <CouponItem/>
                            </Col>
                        </Row>
                    </Col>
                    <Col span={3}/>
                </Row>
                <Row>
                    <Col span={3}/>
                    <Col span={18}>
                        <FootBar/>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        )
    }
}

export default MyCoupons;