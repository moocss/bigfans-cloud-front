import React, {Component} from 'react';
import { Form, Icon, Input, Button, Checkbox , Row , Col } from 'antd';
import HttpUtils from '../../utils/HttpUtils'
import {Link} from 'react-router-dom'

const FormItem = Form.Item;

class LoginForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        let self = this;
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
            let formVals = this.props.form.getFieldsValue();
            HttpUtils.login(formVals , {
                success : function (resp) {
                    if(resp && resp.redirectUrl){
                        self.props.history.push(resp.data.redirectUrl);
                    }
                    self.props.history.push("/");
                } ,
                error : function (resp) {
                    console.info("error: " + resp)
                }
            })
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                <Row>
                    <Col span={3}/>
                    <Col span={6} offset={6}>
                        <Form onSubmit={this.handleSubmit} className="login-form">
                            <FormItem>
                                {getFieldDecorator('account', {
                                    rules: [{ required: true, message: 'Please input your username!' }],
                                })(
                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please input your Password!' }],
                                })(
                                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                                )}
                            </FormItem>
                            <FormItem>
                                {getFieldDecorator('remember', {
                                    valuePropName: 'checked',
                                    initialValue: true,
                                })(
                                    <Checkbox>Remember me</Checkbox>
                                )}
                                <Link className="login-form-forgot" to="">Forgot password</Link><br/>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Log in
                                </Button>
                                Or <Link to="/register">register now!</Link>
                            </FormItem>
                        </Form>
                    </Col>
                    <Col span={3}/>
                </Row>
            </div>
        );
    }
}

const WrappedNormalLoginForm = Form.create()(LoginForm);
export default WrappedNormalLoginForm;